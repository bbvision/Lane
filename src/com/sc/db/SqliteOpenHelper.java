package com.sc.db;
import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.sc.app.Config;
import com.sc.utils.FileUtil;

public class SqliteOpenHelper extends OrmLiteSqliteOpenHelper {
	private final String TAG = this.getClass().getSimpleName();
	/* 在此声明实体类 */
	private Class<?>[] classes = {MarkRecord.class, MarkPoint.class};

	public SqliteOpenHelper(Context context) {
		super(context, FileUtil.getFilePath(), null, Config.DATABASE.VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database,
			ConnectionSource connectionSource) {
		try {
			for (Class<?> clazz : classes) {
				TableUtils.createTableIfNotExists(connectionSource, clazz);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase database,
			ConnectionSource connectionSource, int oldVer, int newVer) {
		try {
			for (Class<?> clazz : classes) {
				TableUtils.dropTable(connectionSource, clazz, true);
			}
			onCreate(database, connectionSource);
		} catch (SQLException e) {
			Log.e(TAG, "Unable to upgrade database from version " + oldVer
					+ " to new " + newVer, e);
			e.printStackTrace();
		}
	}
}