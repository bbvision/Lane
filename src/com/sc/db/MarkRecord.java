package com.sc.db;

import com.j256.ormlite.field.DatabaseField;

public class MarkRecord {
	public static final int MARK_DONE = 0;
	public static final int MARK_INPROGRESS = 1;
	@DatabaseField(generatedId = true)
	int id;
	@DatabaseField
	String starting;
	@DatabaseField
	String destination;
	@DatabaseField
	String description;
	@DatabaseField
	int status;
	
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("id = " + id + "\n");
		builder.append("starting = " + starting + "\n");
		builder.append("destination = " + destination + "\n");
		builder.append("description = " + description + "\n");
		builder.append("status = " + status + "\n");
		return builder.toString();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStarting() {
		return starting;
	}
	public void setStarting(String starting) {
		this.starting = starting;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
