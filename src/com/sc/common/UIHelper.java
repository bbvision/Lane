package com.sc.common;

import greendroid.widget.MyQuickAction;
import greendroid.widget.QuickAction;
import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.sc.app.AppContext;
import com.sc.lane.R;

public class UIHelper {
	/**
	 * �������ʾ��¼��ǳ�
	 * 
	 * @param activity
	 * @param qa
	 */
	public static void showSettingLoginOrLogout(Activity activity,
			QuickAction qa) {
		if (((AppContext) activity.getApplication()).isLogin()) {
			qa.setIcon(MyQuickAction.buildDrawable(activity,
					R.drawable.ic_menu_logout));
			qa.setTitle(activity.getString(R.string.main_menu_logout));
		} else {
			qa.setIcon(MyQuickAction.buildDrawable(activity,
					R.drawable.ic_menu_login));
			qa.setTitle(activity.getString(R.string.main_menu_login));
		}
	}
	
	public static void ToastMessage(Context cont, String msg) {
		Toast.makeText(cont, msg, Toast.LENGTH_SHORT).show();
	}
}
