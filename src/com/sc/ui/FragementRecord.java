package com.sc.ui;

import java.io.Serializable;
import java.util.List;

import com.sc.adapter.RecordsAdapter;
import com.sc.db.DBStorage;
import com.sc.db.MarkPoint;
import com.sc.db.MarkRecord;
import com.sc.lane.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.AdapterView.OnItemLongClickListener;

public class FragementRecord extends Fragment {
	private ListView mlvRecordsList;
	private List<MarkRecord> mRecordsList = null;
	private RecordsAdapter mAdapter;
	private Activity mActivity;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		System.out.println("AAAAAAAAAA____onAttach");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		System.out.println("AAAAAAAAAA____onCreate");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		System.out.println("AAAAAAAAAA____onCreateView");
		View view = inflater.inflate(R.layout.main_fragement_record, container,
				false);
		mlvRecordsList = (ListView) view.findViewById(R.id.lv_records);
		// DBStorage.getString(getActivity().getApplicationContext());

		mActivity = getActivity();

		mRecordsList = DBStorage.getMarkRecords(getActivity()
				.getApplicationContext());
		mAdapter = new RecordsAdapter(getActivity(), mRecordsList);
		mlvRecordsList.setAdapter(mAdapter);
		mlvRecordsList.setOnItemLongClickListener(new OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View arg1, int arg2, long arg3) {
						final int pos = arg2;
						
						/*
						List<MarkPoint> points = DBStorage.getMarkPoints(
								getActivity().getApplicationContext(),
								mRecordsList.get(pos).getId());
						StringBuilder builder = new StringBuilder();
						for (MarkPoint point : points) {
							builder.append(point.toString());
						}

						new AlertDialog.Builder(mActivity).setTitle("标题")
								.setMessage(builder.toString())
								.setPositiveButton("确定", null).show();
								
						*/
						new AlertDialog.Builder(mActivity).setTitle("确认")
						.setMessage("确定删除吗？")
						.setPositiveButton("是", new OnClickListener(){
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								DBStorage.removeMarkRecord(getActivity().getApplicationContext(),
										mRecordsList.get(pos).getId());
								
								mRecordsList.remove(pos);
								mlvRecordsList.invalidateViews();
							}
							
						})
						.setNegativeButton("否", new OnClickListener(){
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
							}
						}).show();
						return true;
					}
				});
		
		mlvRecordsList.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				int pos = arg2;
				List<MarkPoint> points = DBStorage.getMarkPoints(
						getActivity().getApplicationContext(),
						mRecordsList.get(pos).getId());
				Intent intent = new Intent(FragementRecord.this.getActivity(), 
						ViewRecordActivity.class);  
	            intent.putExtra("points", (Serializable)points);  
	            FragementRecord.this.startActivity(intent);   
			}
		});

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		System.out.println("AAAAAAAAAA____onActivityCreated");
	}

	@Override
	public void onStart() {
		super.onStart();
		System.out.println("AAAAAAAAAA____onStart");
	}

	@Override
	public void onResume() {
		super.onResume();
		System.out.println("AAAAAAAAAA____onResume");
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("AAAAAAAAAA____onPause");
	}

	@Override
	public void onStop() {
		super.onStop();
		System.out.println("AAAAAAAAAA____onStop");
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		System.out.println("AAAAAAAAAA____onDestroyView");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		System.out.println("AAAAAAAAAA____onDestroy");
	}

	@Override
	public void onDetach() {
		super.onDetach();
		System.out.println("AAAAAAAAAA____onDetach");
	}
}
